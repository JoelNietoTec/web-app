import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Course } from 'src/app/shared/models/studyplans.model';
import { CoursesService } from 'src/app/shared/services/courses.service';

@Component({
  selector: 'app-course-details',
  templateUrl: './course-details.component.html',
  styleUrls: ['./course-details.component.sass'],
})
export class CourseDetailsComponent implements OnInit {
  course$: Observable<Course>;
  constructor(
    private route: ActivatedRoute,
    public coursesService: CoursesService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(
      (params) => {
        this.course$ = this.coursesService.get(params.id);
      },
      (err) => console.error(err)
    );
  }
}
